// 1. buat array of object students ada 5 data cukup

// 2. 3 function, 1 function nentuin provinsi kalian ada di jawa barat gak, umur kalian diatas 22 atau gak, status kalian single atau gak

// 3. callback function untuk print hasil proses 3 function diatas.
// nama saya imam, saya tinggal di jawa baRAT, umur saya dibawah 22 tahun. dan status saya single loh. CODE buat Nilam


const students = [
    { nama: 'Nur', provinsi: 'Jawa Barat', umur: 21, status: 'single' },
    { nama: 'Aini', provinsi: 'Jawa Timur', umur: 22, status: 'single' },
    { nama: 'Lailla', provinsi: 'Jawa Barat', umur: 24, status: 'single' },
    { nama: 'Asri', provinsi: 'Jawa Tengah', umur: 26, status: 'maried' },
    { nama: 'Dita', provinsi: 'Jawa Barat', umur: 21, status: 'maried' },
]

function getProvinsi(i, data) {
    if (data[i].provinsi === 'Jawa Barat') {
        return "tinggal di Provinsi Jawa Barat";
    }
    else {
        return `tidak tinggal di Provinsi Jawa Barat tetapi di ${data[i].provinsi}`;
    }
}

function getUmur(i, data) {
    if (data[i].umur < 22) {
        return "kurang dari 22 tahun";
    } else {
        return "lebih dari 22 tahun";
    }
}

function getStatus(i, data) {
    if (data[i].status === 'single') {
        return "single";
    }
    else {
        return "maried";
    }
}

function getData(data, callback) {
    for (let i = 0; i < data.length; i++) {
        callback(data[i].nama, getProvinsi(i, data), getUmur(i, data), getStatus(i, data));
    }
}

getData(students, (nama, provinsi, umur, status) => {
    console.log(`Nama saya ${nama}, saya ${provinsi}, umur saya ${umur}, dan status saya ${status}`);
})


